module otsuThreshold(
		input clk, 
		input rst,
		input start, 
		input [7:0] in, 
		input [127:0] threshold, 
		input toOtsuThreshold, 
		input byte unsigned q_a, 
		output bit [13:0] address_a = 0, 
		output bit [13:0] address_b = 0, 
		output byte unsigned data_b, 
		output bit wren_b, 
		output bit toHistogramBins = 0
	);
	parameter SHIFT_AMOUNT = 24;
	parameter SHIFT_MASK = (1 << SHIFT_AMOUNT) - 1;
	
	shortint count = 0, i = 0, j = 0;
	byte state = 0;
	
	integer data_file;
	integer print_file;
	
	initial begin
		// data_file = $fopen("otsuThreshold.txt", "w");
		// if (data_file == 0) begin
			// $display("data_file handle was NULL");
			// $finish;
		// end
	end

	always_ff @(posedge clk) begin
		if (rst) begin
			count <= 0;
			i <= 0;
			j <= 0;
			address_a <= 0;
			address_b <= 0;
			toHistogramBins <= 0;
			state <= 0;
		end
		
		case (state)
		0: begin
			if (start & count < 16384) begin
				address_b <= 14'(i);
				data_b <= in[7:0];
				wren_b <= 1;
				// $fwrite(data_file,"%d %h\n",i,in[7:0]);
				
				count <= count + 1;
				state <= state + 1;
			end
			if (count >= 16384) begin
				state <= 2;
			end
		end
		
		1: begin
			if (wren_b) begin
				wren_b <= 0;
			end
			i <= i + 1;
			state <= state - 1;
		end
		
		2: begin
			if (toOtsuThreshold) begin
				if (j < 16384 + 1) begin
					if (j < 16384) begin
						address_a <= 14'(j);
					end
					
					// $fwrite(data_file,"%d ",j);
					// $fwrite(data_file,"%h ",q_a);
			
					state <= state + 1;
				end else begin
				
				// $fclose(data_file);
					toHistogramBins <= 1;
					address_a <= 0;
					address_b <= 0;
				end
			end
		end
		
		3: begin
			if (j >= 1) begin
				address_b <= 14'(j - 1);
				wren_b <= 1;
					
				// $fwrite(data_file,"%d ",j-1);
				// $fwrite(data_file,"%h ",q_a);
				
				if (q_a >= threshold) begin
					data_b <= 1;
					// $fwrite(data_file,"1\n");
				end else begin
					data_b <= 0;
					// $fwrite(data_file,"0\n");
				end
			end
		
			state <= state + 1;
		end
		
		4: begin
			// $fwrite(data_file, "j=%d pixel=%h out=%b\n", (j-1), q_a, data_b[0]);
			if (wren_b) begin
				wren_b <= 0;
			end
			j <= j + 1;
			state <= 2;
		end
		endcase
	end
endmodule