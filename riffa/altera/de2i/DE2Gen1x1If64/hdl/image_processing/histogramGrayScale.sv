module histogramGrayScale (
		input clk,
		input rst,
		input start, 
		input [7:0] in,
		output bit [127:0] out, 
		output bit toOtsuThreshold = 0
	);
	parameter GRAYSCALE_LEVELS = 255;
	parameter SHIFT_AMOUNT = 24;
	parameter SHIFT_MASK = (1 << SHIFT_AMOUNT) - 1;

	//é preciso ter um a mais para enviar em pares
	bit [GRAYSCALE_LEVELS:0][15:0] histogram = 4096'b0; //256*16 = 4096/4 = 1mb
	shortint i = 0, j = 0, count = 0;
	byte state = 0;
	int wB = 0, wF, totalPixels = 32'd16384;
	bit signed [127:0] backgroundSum = 0, meanBackground, meanForeground, betweenClassVariance, maxVariance = 0, weightedSum = 0, temp;
	
 	integer data_file;
	integer scan_file;
	
	initial begin
 		// data_file = $fopen("histogramGrayScale.txt", "w");
		// if (data_file == 0) begin
			// $display("data_file handle was NULL");
			// $finish;
		// end
	end
	
	always_ff @(posedge clk) begin
		if (rst) begin
			histogram <= 4096'b0;
			i <= 0;
			j <= 0;
			weightedSum <= 0;
			wB <= 0;
			maxVariance <= 0;
			backgroundSum <= 0;
			toOtsuThreshold <= 0;
			count <= 0;
			state <= 0;
			//toOtsuLevel = 0;
		end
		
		case (state)
		0: begin
			if (start & count < 16384) begin
				histogram[in] <= histogram[in] + 1;
				count <= count + 1;
				// $fwrite(data_file,"in:%h\n",in);
			end 
			if (count >= 16384) begin
				state <= state + 1;
			end
		end
		
		1: begin
			if (i < GRAYSCALE_LEVELS) begin
				temp <= ((64'((i << SHIFT_AMOUNT) << SHIFT_AMOUNT) / (255 << SHIFT_AMOUNT)) * (histogram[i] << SHIFT_AMOUNT)) >>> SHIFT_AMOUNT;
				// temp = temp + ((temp & 1 << (SHIFT_AMOUNT - 1)) << 1);
				// temp >>>= SHIFT_AMOUNT;
				// weightedSum = weightedSum + temp;
				
				// $fwrite(data_file,"histogram[%d]:%d\n",i,histogram[i]);
				state <= state + 1;
			end else begin
				state <= 3;
			end
		end
		
		2: begin
			weightedSum <= weightedSum + temp;
			i <= i + 1;
			// $fwrite(data_file,"weightedSum:%h\n", weightedSum);
			state <= state - 1;
		end
		
		3: begin
			if (j < GRAYSCALE_LEVELS) begin
				wB <= wB + histogram[j];
				temp <= ((64'((j << SHIFT_AMOUNT) << SHIFT_AMOUNT) / (255 << SHIFT_AMOUNT)) * (histogram[j] << SHIFT_AMOUNT)) >>> SHIFT_AMOUNT;
				
			// $fwrite(data_file,"maxVariance:%h\n", maxVariance);
			// $fwrite(data_file,"out:%d\n", out);
				// $fwrite(data_file,"j:%d\n", j);
				state <= state + 1;
			end else begin
				state <= 8;
			end
		end
		
		4: begin
			if (wB != 0) begin
				wF <= totalPixels - wB;
				backgroundSum <= backgroundSum + temp;
				state <= state + 1;
			end else begin
				state <= 3;
			end
			j <= j + 1;
		end
		
		5: begin
			if (wF != 0) begin
				//meanBackground = backgroundSum / wB;
				meanBackground <= 64'(backgroundSum << SHIFT_AMOUNT) / (wB << SHIFT_AMOUNT);
				//meanForeground = (weightedSum - backgroundSum) / wF;
				meanForeground <= 64'((weightedSum - backgroundSum) << SHIFT_AMOUNT) / (wF << SHIFT_AMOUNT);
				//betweenClassVariance = (double)wB * (double)wF * (meanBackground - meanForeground) * (meanBackground - meanForeground);
				temp <= ((wB << SHIFT_AMOUNT) * (wF << SHIFT_AMOUNT)) >>> SHIFT_AMOUNT;
				state <= state + 1;
				// $fwrite(data_file,"wB:%d\n", wB);
				// $fwrite(data_file,"wF:%d\n", wF);
				// $fwrite(data_file,"backgroundSum:%h\n", backgroundSum);
			end else begin
				state <= 3;
			end
		end
		
		6: begin
			betweenClassVariance <= (((temp * (meanBackground - meanForeground)) >>> SHIFT_AMOUNT) * (meanBackground - meanForeground)) >>> SHIFT_AMOUNT;
			state <= state + 1;
			// $fwrite(data_file,"meanBackground:%h\n", meanBackground);
			// $fwrite(data_file,"meanForeground:%h\n", meanForeground);
		end
		
		7: begin
			if (betweenClassVariance > maxVariance) begin
				maxVariance <= betweenClassVariance;
				out <= j - 1;
			end
			state <= 3;
			// $fwrite(data_file,"betweenClassVariance:%h\n", betweenClassVariance);
		end
		
		8: begin
			toOtsuThreshold <= 1;
			state <= state + 1;
			// $fwrite(data_file,"end\n");
			// $stop;
		end
		endcase
	end
endmodule
