# Scene Text Character Recognition (STCR)

The project presented to the Undergraduate Program in Computer Engineering of the Computer Science Center of the Federal University of Pernambuco as a partial requirement to obtain a Bachelor's Degree in Computer Engineering.

Advisor: Edna Natividade da Silva Barros

Abstract: High-precision character recognition algorithms in natural scenes (STCR) require high processing power. To execute these algorithms on a real-time embedded system satisfactorily is required a heterogeneous architecture composed of general-purpose processors and single-purpose processors. Thus, we allocated the stages of the STCR technique to hardware components (single-purpose processors) and software (general purpose processors). We dedicated stages that require most processing power to the hardware component of the system, in order to reduce the total execution time of the algorithm. Among a strategy defined by the research group, one of the steps of reasonable computational cost consists of the HOG (Histogram of Oriented Objects). We also implemented the Otsu Thresholding in the hardware component.

Keywords: FPGA, image processing, scene text character recognition, hardware accelerator, embedded systems, computer vision

This folder is for deployment into the Intel protoboard DE2i-150 FPGA Development Kit. 

In the folder (resize-otsu-hog/riffa/altera/de2i/DE2Gen1x1If64/hdl/image_processing) can be found the two modules implemented in this project besides the control modules.  The first module representing the HOG is the file HistogramBins.sv and the second one is OtsuThreshold.sv. We implemented all modules here in SystemVerilog or Verilog.